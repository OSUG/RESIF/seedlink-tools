/*

SEE : http://ds.iris.edu/ds/nodes/dmc/data/latency/

Input Latency Measurements

Measures of Latency for data entering the DMC. These measures are from the perspective of data entering the IRIS BUD system.

DMC Input Data Latency (T A – T D) is defined to be the difference in time from when the BUD system receives a data record and 
the time of the last sample in the data record for which latency is being measured. The measure of Input Data Latency does not change for a given data record.

DMC Input Feed Latency (T M – T A) is defined to be the difference in time from when the feed latency measurement is made 
to the last time some data from a given channel becomes available in the BUD system. 
The Input Feed latency can change with time and depends upon when the feed latency measurement is made (T M).

DMC Input Total Latency (T M – T D) is defined to be the difference in 
time from when the feed latency measurement is made and the time of the last sample in the data record. 
It should be obvious that the Input Total Latency is the sum of the Input Data Latency and the Input Feed Latency.


Output Latency Measurements

Measures of Latency for data leaving the DMC. Output Latency measurements are specific to a particular IRIS DMC data server, such as the SeedLink server.

Operational downtime may result in latency being introduced within the DMC. Server downtime or telecommunication problems could introduce latencies of this type.

DMC Output Data Latency (T A – T D) is defined to be the difference in time from when a data record is available from a data server 
at the IRIS DMC and the time of the last sample in the data record in question. 
The measure of Output Data Latency does not change for a given data record.

DMC Output Feed Latency (T M – T A) is defined to be the difference in time from when the feed latency measurement 
is made to the last time some data from a given channel was available from the data server. 
The measure of Output Feed Latency will change with time and depends upon when the feed latency measurement is made (T M).

DMC Output Total Latency (T M – T D) is defined to be the difference in time from when the feed latency measurement 
is made and the time of the last sample in the data record. It should be obvious that the Output Total Latency 
is the sum of the Output Data Latency and the Output Feed Latency.
Frequency of Measurements

As of July 2007 the IRIS DMC was receiving data from approximately 15,000 seismic channels. It is computationally impossible to make latency 
measurements for every data record for all of these channels. For this reason, it is the DMC’s approach to take snapshots of latencies for 
all channels on the order of every few hours. The rate at which latency is measured may vary within different systems.

==>RESIF : janvier 2017 : on decide de scanner les outputs de rtserve tous les 1/4 heures
*/

/*
* Table d'input pour le stockage des données en entrée du noeudB, destinée à la mesure des 
 
 TABLE "latency_in"( 		
"filename" TEXT, 				-- Nom du fichiers
"date_segment" text,  		-- TD
"date_file" text, 			-- TA
"date_run" text);				-- TM

TABLE "latency_in_bis"(
	"filename" TEXT, 				-- File name
	"date_segment" text,  		-- TD, unformatted
	"date_file" text, 			-- TA, unformatted
	"date_run" text,				-- TM, unformatted
	"network" TEXT, 
	"station" TEXT,  
	"location" TEXT, 
	"channel" TEXT, 
	"TD" TIMESTAMP WITHOUT TIME ZONE, -- TD represents the time of the last sample in a data record
	"TA" TIMESTAMP WITHOUT TIME ZONE, --TA represents the time the data record became available. This could be the time 
												 -- a data record was written to disk
	"TM" TIMESTAMP WITHOUT TIME ZONE, -- TM represents the wall clock time a feed latency measurement was made.
	"dataLatency" INTERVAL, 			-- Input Data Latency (T A – T D) is defined to be the difference in time from when the BUD system receives 
												-- a data record and the time of the last sample in the data record for which latency is being measured. 
												-- The measure of Input Data Latency does not change for a given data record.
	"feedLatency" INTERVAL, 			-- Input Feed Latency (T M – T A) is defined to be the difference in time from when the feed latency measurement is made 
												-- to the last time some data from a given channel becomes available in the BUD system. 
												-- The Input Feed latency can change with time and depends upon when the feed latency measurement is made (T M).
	"totalLatency" INTERVAL); 			-- Input Total Latency (T M – T D) is defined to be the difference in time from when the feed latency 
												-- measurement is made and the time of the last sample in the data record. 
												-- It should be obvious that the Input Total Latency is the sum of the Input Data Latency and the Input Feed Latency.
*/

DELETE FROM "latency_in_bis";
-- Cette table contient un nuplet par fichier de données 
-- lors d'une mise a jour des tables des latences, cette table contient pluiseurs lignes résultant d'un balayage fichier par fichier du repertoire
-- de dépot des données TR collectées par les clients slinktool. 
-- le balayage des fichier est exectue toutes les x minutes.
-- les valeurs TD, TA et TM voir ci-dessus sont identifiée par le script de balayage

INSERT INTO "latency_in_bis" SELECT DISTINCT
	l."filename",
	l."date_segment",
	l."date_file", 
	l."date_run",
	SPLIT_PART(l.filename,'.',1), -- network
	SPLIT_PART(l.filename,'.',2), -- station
	SPLIT_PART(l.filename,'.',3), -- location
	SPLIT_PART(l.filename,'.',4), -- channel
	to_timestamp(l.date_segment,'YYYY,DDD,HH24:MI:SS.US'), -- TD
	to_timestamp(l.date_file,'YYYY-MM-DD HH24:MI:SS.US'), -- TA 2015-08-26 09:22:47.567559842
	to_timestamp(l.date_run,'YYYY,DDD,HH24:MI:SS.US'), -- TM 2015,238,11:06:45.1440587205
	'0 sec'::INTERVAL,
	'0 sec'::INTERVAL,
	'0 sec'::INTERVAL
FROM "latency_in" l; 

--
-- définir une date d'exécution de commande (fictive) unique pour tous les couples network, station
-- la date provenant du scen des fichiers est mise a jour a chaque scan
--
CREATE OR REPLACE  FUNCTION  max_tm_1(TEXT,TEXT) RETURNS TIMESTAMP WITHOUT TIME ZONE AS $$
	DECLARE
		net			ALIAS FOR $1;			
		stat			ALIAS FOR $2;
		date			TIMESTAMP WITHOUT TIME ZONE;
	BEGIN
		SELECT MAX("latency_in_bis"."TM")
		FROM "latency_in_bis" 
		WHERE 
			net="latency_in_bis"."network" AND stat="latency_in_bis"."station"
		INTO date;
		RETURN date;
	END;$$
LANGUAGE 'plpgsql';

CREATE OR REPLACE  FUNCTION  max_ta_1(TEXT,TEXT) RETURNS TIMESTAMP WITHOUT TIME ZONE AS $$
	DECLARE
		net			ALIAS FOR $1;			
		stat			ALIAS FOR $2;
		date			TIMESTAMP WITHOUT TIME ZONE;
	BEGIN
		SELECT MAX("latency_in_bis"."TA")
		FROM "latency_in_bis" 
		WHERE 
			net="latency_in_bis"."network" AND stat="latency_in_bis"."station"
		INTO date;
		RETURN date;
	END;$$
LANGUAGE 'plpgsql';
CREATE OR REPLACE  FUNCTION  max_td_1(TEXT,TEXT) RETURNS TIMESTAMP WITHOUT TIME ZONE AS $$
	DECLARE
		net			ALIAS FOR $1;			
		stat			ALIAS FOR $2;
		date			TIMESTAMP WITHOUT TIME ZONE;
	BEGIN
		SELECT MAX("latency_in_bis"."TD")
		FROM "latency_in_bis" 
		WHERE 
			net="latency_in_bis"."network" AND stat="latency_in_bis"."station"
		INTO date;
		RETURN date;
	END;$$
LANGUAGE 'plpgsql';

ALTER TABLE "latency_in_bis" DROP COLUMN IF EXISTS max_tm_station CASCADE;
ALTER TABLE "latency_in_bis" ADD COLUMN max_tm_station TIMESTAMP WITHOUT TIME ZONE;
ALTER TABLE "latency_in_bis" DROP COLUMN IF EXISTS max_ta_station CASCADE;
ALTER TABLE "latency_in_bis" ADD COLUMN max_ta_station TIMESTAMP WITHOUT TIME ZONE;
ALTER TABLE "latency_in_bis" DROP COLUMN IF EXISTS max_td_station CASCADE;
ALTER TABLE "latency_in_bis" ADD COLUMN max_td_station TIMESTAMP WITHOUT TIME ZONE;

UPDATE "latency_in_bis" SET max_tm_station = max_tm_1 (network,station);
UPDATE "latency_in_bis" SET max_ta_station = max_ta_1 (network,station);
UPDATE "latency_in_bis" SET max_td_station = max_td_1 (network,station);

UPDATE "latency_in_bis" SET "dataLatency"="TA" - "TD";
UPDATE "latency_in_bis" SET "feedLatency"="TM" - "TA";
UPDATE "latency_in_bis" SET "totalLatency"="dataLatency" + "feedLatency" ;
--
-- dans la table "ALLlatency"
-- détruire les nuplets > 6 jours 
-- 
DELETE FROM "ALLlatency" WHERE current_timestamp - "TM" >= CAST ('6 days' AS INTERVAL);
--
-- Ajouter les mesures courantes à la table  "ALLlatency" et à la table "ALLlatencyStore"
--
INSERT INTO "ALLlatency" SELECT DISTINCT 
	a.network,
	a.station,
	a.location,
	a.channel,
	a."TD",
	a."TA",
	a."TM",
	a."dataLatency",
	a."feedLatency",
	a."totalLatency"
FROM "latency_in_bis" a;
INSERT INTO "ALLlatencyStore" SELECT DISTINCT 
	a.network,
	a.station,
	a.location,
	a.channel,
	a."TD",
	a."TA",
	a."TM",
	a."dataLatency",
	a."feedLatency",
	a."totalLatency"
FROM "latency_in_bis" a;
--
-- Mettre a jour les vues pour l'interface budmonitor
--
ALTER TABLE 
DELETE FROM "LASTlatency";
INSERT INTO "LASTlatency" (network,station,location, channel,"TD","TA","TM", "dataLatency", "feedLatency" , "totalLatency")
SELECT DISTINCT 
	a.network,a.station,a.location, a.channel,a."TD",a."TA",a."TM", a."dataLatency", a."feedLatency" , "totalLatency"
FROM "latency_in_bis" a;

DELETE FROM "LASTStationlatency";

INSERT INTO "LASTStationlatency" (network,station,"TD","TA","TM")
SELECT DISTINCT 
	a.network,
	a.station,
	a.max_td_station,
	a.max_ta_station,
	a.max_tm_station --,
	--a."dataLatency",
	--a."feedLatency",
	--a."totalLatency"
FROM "latency_in_bis" a;
UPDATE "LASTStationlatency" SET "dataLatency"="TA" - "TD";
UPDATE "LASTStationlatency" SET "feedLatency"="TM" - "TA";
UPDATE "LASTStationlatency" SET "totalLatency"="dataLatency" + "feedLatency" ;

---------------------------------------------------------------------------------------------------------------------------------
-- Pour le stockage des latences en sortie, récupérées par le script de monitoring
-- Les tables suivantes doivent être mises à jour
-- "ALLlatency_out"
-- "ALLlatencyStore_out"
--
-- la mise à jour se fera surement par nuplet et non par lot
-- 
-- il faudra donc redefinir "LASTlatency" et "LASTStationlatency" comme des vues matérialisées
-- rafraichise a chaque mesure ....

--  
-- Creation de la vue "LASTlatency" à partir de 

-- on ne garde que les nuplets pour lesquels la valeur TM est la valeur max des TM

INSERT INTO "LASTStationlatency" (network,station,"TD","TA","TM")
SELECT DISTINCT 
	a.network,
	a.station,
	a.max_td_station,
	a.max_ta_station,
	a.max_tm_station --,
	--a."dataLatency",
	--a."feedLatency",
	--a."totalLatency"
FROM "latency_in_bis" a;
UPDATE "LASTStationlatency" SET "dataLatency"="TA" - "TD";
UPDATE "LASTStationlatency" SET "feedLatency"="TM" - "TA";
UPDATE "LASTStationlatency" SET "totalLatency"="dataLatency" + "feedLatency" ;



