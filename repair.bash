#!/bin/bash
# set -x
# $HOME/scripts/repair.bash
## [Fev. 26, 2016 C. Péquegnat ] : création du script
# Mise a jour de resifInv-Prod.PUBLIC.rbud par cron (et non par notify)

. $HOME/.bash_profile
set -u
export PGHOST="postgres-geodata.ujf-grenoble.fr"
export PGUSER="resifinvprod"
export PGPASSFILE="$HOME/.pgpass"
export BD="resifInv-Prod"
export NETWORK="";
while getopts "d:n:" flag; do
    case $flag in
        d) export DAY=$OPTARG ;;
        n) export NETWORK=$OPTARG ;;
    esac
done
shift $((OPTIND-1))
# $DAY is mandatory
if [[ -z $DAY ]] 
	then
		echo "Syntax: "
		exit 2
fi
# $NETWORK is optional
export YEAR=`echo $DAY | cut -d'.' -f 1`
cd /mnt/auto/archive/rawdata/bud/$YEAR/$NETWORK
find . -type f -name "*$DAY" -print | while read line; do
	echo $line
	export "filename"=`echo $line | cut -d' ' -f 7`
	export "name"=`basename $filename`
	export "net"=`echo $name | cut -d'.' -f 1`
	export "station"=`echo $name | cut -d'.' -f 2`
	export "location"=`echo $name | cut -d'.' -f 3`
	export "channel"=`echo $name | cut -d'.' -f 4`
	export "type"=`echo $name | cut -d'.' -f 5`
	export "year"=`echo $name | cut -d'.' -f 6`
	export "day"=`echo $name | cut -d'.' -f 7`
	echo "/home/sysop/bin/msi -p $filename | grep $net| cut -d',' -f 3 | sort -u"
	export "quality"=`/home/sysop/bin/msi -p $filename | grep $net | cut -d',' -f3 | sort -u | sed "s/ //g"`
	echo "$filename,$name,$net,$quality,$day"
# 
# seuls les flux de données (type = 'D') sont répertoriés dans la base de données
#
if [ "$type" = "D" ] 
	then
	echo "`date` : integrating $name"

psql -d ${BD} -c " \
	INSERT INTO  rbud (rbud_id, network, station, location ,channel,  starttime, endtime, source_file, quality, block_size, year ) \
	SELECT nextval('rbud_id_seq'), '$net','$station', '$location', '$channel',  \
	TO_TIMESTAMP(SPLIT_PART('$name','.', 6) || '-' ||SPLIT_PART('$name','.', 7), 'YYYY-DDD'), \
	TO_TIMESTAMP(SPLIT_PART('$name','.', 6) || '-' ||SPLIT_PART('$name','.', 7), 'YYYY-DDD') + CAST ('24h' AS INTERVAL), \
	'$name', '$quality','512', '$year'
	WHERE \
	NOT EXISTS (SELECT  b.source_file FROM  rbud b WHERE source_file = '$name') AND \
	SPLIT_PART('$name','.', 6) || '.' ||SPLIT_PART('$name','.', 7) = TO_CHAR (CURRENT_TIMESTAMP,'YYYY.DDD');"
	CR=$?
	[[ "$CR" -ne "0" ]] && echo "`date` WARNING psql INSERT $name return $CR" &&  echo "PB acces SGBD a monitorer"
	psql -d ${BD} -c " \
	UPDATE rbud SET channel_id = set_channel_id_in_file_table (network,station,location,channel, starttime, endtime) WHERE source_file = '$name' ; \
	UPDATE rbud SET location = '--' where location = ''; \
	UPDATE rbud SET location = '--' where location = ' '; \
	UPDATE rbud SET channel_id = set_channel_id_in_file_table (network,station,location,channel, starttime, endtime) WHERE source_file = '$name' ;"
	[[ "$CR" -ne "0" ]] && echo "`date` WARNING psql update rbud  with $name return $CR" &&  echo "PB acces SGBD a monitorer"
	
#
#	echo "INSERT INTO  rbud (rbud_id, network, station, location ,channel,  starttime, endtime, source_file, quality, block_size, year ) \
#	SELECT nextval('rbud_id_seq'), '$net','$station', '$location', '$channel',  \
#	TO_TIMESTAMP(SPLIT_PART('$name','.', 6) || '-' ||SPLIT_PART('$name','.', 7), 'YYYY-DDD'), \
#	TO_TIMESTAMP(SPLIT_PART('$name','.', 6) || '-' ||SPLIT_PART('$name','.', 7), 'YYYY-DDD') + CAST ('24h' AS INTERVAL), \
#	'$name', '$quality','512', '$year'
#	WHERE \
#	NOT EXISTS (SELECT  b.source_file FROM  rbud b WHERE source_file = '$name') AND \
#	SPLIT_PART('$name','.', 6) || '.' ||SPLIT_PART('$name','.', 7) = TO_CHAR (CURRENT_TIMESTAMP,'YYYY.DDD');
#	UPDATE rbud SET channel_id = set_channel_id_in_file_table (network,station,location,channel, starttime, endtime) \
#	WHERE source_file = '$name' ;" 
#
# Check if exists in rbud
#
	psql -d ${BD} -t -c "SELECT DISTINCT * FROM rbud WHERE source_file = '$name';"
	CR=$?
	[[ "$CR" -ne "0" ]] && echo "`date` WARNING psql SELECT $name return $CR" && echo "PB acces SGBD a monitorer"
	else
echo "`date` : $name not integrated"
fi
done

