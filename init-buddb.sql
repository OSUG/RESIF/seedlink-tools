-- 
-- Latency measurement at RESIF-DC
-- Database : budresifb
-- Tables : 

/*
SEE : http://ds.iris.edu/ds/nodes/dmc/data/latency/

TD represents the time of the last sample in a data record
TA represents the time the data record became available. This could be the time 
a data record was written to disk or the time a data record became available in memory.
TM represents the wall clock time a feed latency measurement was made.

DMC Input Data Latency (T A – T D) is defined to be the difference in time from when the BUD system receives 
a data record and the time of the last sample in the data record for which latency is being measured. 
The measure of Input Data Latency does not change for a given data record.

DMC Input Feed Latency (T M – T A) is defined to be the difference in time from when the feed latency measurement is made 
to the last time some data from a given channel becomes available in the BUD system. 
The Input Feed latency can change with time and depends upon when the feed latency measurement is made (T M).

DMC Input Total Latency (T M – T D) is defined to be the difference in time from when the feed latency 
measurement is made and the time of the last sample in the data record. 
It should be obvious that the Input Total Latency is the sum of the Input Data Latency and the Input Feed Latency.
*/

/*
Output Latency Measurements

Measures of Latency for data leaving the DMC. Output Latency measurements are specific to a particular IRIS DMC data server, such as the SeedLink server.

Operational downtime may result in latency being introduced within the DMC. Server downtime or telecommunication problems could introduce latencies of this type.

DMC Output Data Latency (T A – T D) is defined to be the difference in time from when a data record is available from a data server at the IRIS DMC and the time of the last sample in the data record in question. The measure of Output Data Latency does not change for a given data record.

DMC Output Feed Latency (T M – T A) is defined to be the difference in time from when the feed latency measurement is made to the last time some data from a given channel was available from the data server. The measure of Output Feed Latency will change with time and depends upon when the feed latency measurement is made (T M).

DMC Output Total Latency (T M – T D) is defined to be the difference in time from when the feed latency measurement is made and the time of the last sample in the data record. It should be obvious that the Output Total Latency is the sum of the Output Data Latency and the Output Feed Latency.

*/

DROP TABLE IF EXISTS "go_in" CASCADE;
CREATE TABLE "go_in" (
nslc	TEXT,
startime TIMESTAMP WITHOUT TIME ZONE,
endtime TIMESTAMP WITHOUT TIME ZONE,
gapoverlap	TEXT,
sampling REAL,
nb_sample	INTEGER)
;

DROP TABLE IF EXISTS "go_out" CASCADE;
CREATE TABLE "go_out" (
nslc	TEXT,
startime TIMESTAMP WITHOUT TIME ZONE,
endtime TIMESTAMP WITHOUT TIME ZONE,
gapoverlap	TEXT,
sampling REAL,
nb_sample	INTEGER)
;

DROP TABLE IF EXISTS "ALLlatency_out" CASCADE;
CREATE TABLE "ALLlatency_out" (
	id SERIAL PRIMARY KEY,
	"network" TEXT, 
	"station" TEXT,  
	"location" TEXT, 
	"channel" TEXT, 
	"TD" TIMESTAMP WITHOUT TIME ZONE,  -- data datetime, endtime in xml file when getting streams with obspy
	"TA" TIMESTAMP WITHOUT TIME ZONE,  -- date of the 'instert into ... ' SQL command
	"TM" TIMESTAMP WITHOUT TIME ZONE,  -- date of the data measurement
	"dataLatency" INTERVAL, 
	"feedLatency" INTERVAL, 
	"totalLatency" INTERVAL);

COMMENT ON TABLE "All latency measurement" IS ' All latency measurement, during ~ 2 months during ~ 2 months';
COMMENT ON COLUMN  "ALLlatency_out"."network" IS 'network code FDSN ';
COMMENT ON COLUMN  "ALLlatency_out"."station" IS 'station';
COMMENT ON COLUMN  "ALLlatency_out"."location" IS 'location ';
COMMENT ON COLUMN  "ALLlatency_out"."channel" IS 'channel ';
COMMENT ON COLUMN  "ALLlatency_out"."TD" IS 'TD represents the time of the last sample in a data record (date dans la donnée dans le cas d un fichier, endtime dans le fichier xml obspy)';
COMMENT ON COLUMN  "ALLlatency_out"."TA" IS 'TA represents the time the data record became available. This could be the time  a data record was written to disk (latence en sortie : date d insertion dans la base) ';
COMMENT ON COLUMN  "ALLlatency_out"."TM" IS 'TM represents the wall clock time a feed latency measurement was made. (latence en sortie : date d exécution de la commande) ';
COMMENT ON COLUMN  "ALLlatency_out"."dataLatency" IS 'Output Data Latency (T A – T D) is defined to be the difference in time from when a data record is available from a data server at the RESIF DMC and the time of the last sample in the data record in question. The measure of Output Data Latency does not change for a given data record';
COMMENT ON COLUMN  "ALLlatency_out"."feedLatency" IS 'Output Feed Latency (T M – T A) is defined to be the difference in time from when the feed latency measurement is made to the last time some data from a given channel was available from the data server. The measure of Output Feed Latency will change with time and depends upon when the feed latency measurement is made (T M)';
COMMENT ON COLUMN  "ALLlatency_out"."totalLatency" IS 'Output Total Latency (T M – T D) is defined to be the difference in time from when the feed latency measurement is made and the time of the last sample in the data record. It should be obvious that the Output Total Latency is the sum of the Output Data Latency and the Output Feed Latency.';
--
-- !!!! Min/Maj in identifiers are not respected in the python script : /etc/zabbix/externalscripts/monitoring_latence/start.py
-- So "ALLlatency_out" is refered as "alllatency_out" when used to update view "LASTStationlatency" and ""LASTStationlatency"
--
-- 
CREATE OR REPLACE  FUNCTION  max_tm_station(TEXT,TEXT) RETURNS TIMESTAMP WITHOUT TIME ZONE AS $$
	DECLARE
		net			ALIAS FOR $1;			
		stat			ALIAS FOR $2;
		date			TIMESTAMP WITHOUT TIME ZONE;
	BEGIN
		SELECT MAX("alllatency_out"."tm")
		FROM "alllatency_out" 
		WHERE 
			net="alllatency_out"."network" AND stat="alllatency_out"."station"
		INTO date;
		RETURN date;
	END;$$
LANGUAGE 'plpgsql';

CREATE OR REPLACE  FUNCTION  min_tm_station(TEXT,TEXT) RETURNS TIMESTAMP WITHOUT TIME ZONE AS $$
	DECLARE
		net			ALIAS FOR $1;			
		stat			ALIAS FOR $2;
		date			TIMESTAMP WITHOUT TIME ZONE;
	BEGIN
		SELECT MIN("alllatency_out"."tm")
		FROM "alllatency_out" 
		WHERE 
			net="alllatency_out"."network" AND stat="alllatency_out"."station"
		INTO date;
		RETURN date;
	END;$$
LANGUAGE 'plpgsql';


CREATE OR REPLACE  FUNCTION  max_tm_channel(TEXT,TEXT,TEXT, TEXT) RETURNS TIMESTAMP WITHOUT TIME ZONE AS $$
	DECLARE
		net			ALIAS FOR $1;			
		stat			ALIAS FOR $2;
		loc			ALIAS FOR $3;
		cha			ALIAS FOR $4;
		date			TIMESTAMP WITHOUT TIME ZONE;
	BEGIN
		SELECT MAX("alllatency_out"."tm")
		FROM "alllatency_out" 
		WHERE 
			net="alllatency_out"."network" AND 
			stat="alllatency_out"."station"	AND
			loc="alllatency_out"."location"	AND
			cha="alllatency_out"."channel"	
		INTO date;
		RETURN date;
	END;$$
LANGUAGE 'plpgsql';

CREATE OR REPLACE  FUNCTION  min_tm_channel(TEXT,TEXT,TEXT, TEXT) RETURNS TIMESTAMP WITHOUT TIME ZONE AS $$
	DECLARE
		net			ALIAS FOR $1;			
		stat			ALIAS FOR $2;
		loc			ALIAS FOR $3;
		cha			ALIAS FOR $4;
		date			TIMESTAMP WITHOUT TIME ZONE;
	BEGIN
		SELECT MIN("alllatency_out"."tm")
		FROM "alllatency_out" 
		WHERE 
			net="alllatency_out"."network" AND 
			stat="alllatency_out"."station"	AND
			loc="alllatency_out"."location"	AND
			cha="alllatency_out"."channel"	
		INTO date;
		RETURN date;
	END;$$
LANGUAGE 'plpgsql';

--
--  "LASTStationlatency"  and LASTlatency" are updated after latencies collection
--  tables used by the application : seismology.resif.fr/budmonitor
--

DROP TABLE IF EXISTS "LASTStationlatency" CASCADE;
DROP TABLE IF EXISTS "LASTlatency" CASCADE;
create table  "LASTStationlatency" AS SELECT DISTINCT
	a.network || a.station as keytext,
	a.network,
	a.station,
	NULL::timestamp without time zone AS "TD",
	NULL::timestamp without time zone AS "TA",
	NULL::timestamp without time zone AS "TM",
	'0 sec'::INTERVAL as "dataLatency",
	'0 sec'::INTERVAL as "feedLatency",
	'0 sec'::INTERVAL as "totalLatency"
FROM "alllatency_out" a;

update "LASTStationlatency" SET 
	"TM" = min_tm_station(network, station);

update "LASTStationlatency" SET 
	"TD" = "a".td 
	FROM "alllatency_out" a
	WHERE
	a.network = "LASTStationlatency".network AND
	a.station = "LASTStationlatency".station AND
	a.tm= "LASTStationlatency"."TM";

update "LASTStationlatency" SET 
	"TA" = "a".td 
	FROM "alllatency_out" a
	WHERE
	a.network = "LASTStationlatency".network AND
	a.station = "LASTStationlatency".station AND
	a.tm= "LASTStationlatency"."TM" AND
	a.td= "LASTStationlatency"."TD" ;

/*
DMC Output Data Latency (T A – T D) is defined to be the difference in time from when a data record is available from a data server at the IRIS DMC and the time of the last sample in the data record in question. The measure of Output Data Latency does not change for a given data record.

DMC Output Feed Latency (T M – T A) is defined to be the difference in time from when the feed latency measurement is made to the last time some data from a given channel was available from the data server. The measure of Output Feed Latency will change with time and depends upon when the feed latency measurement is made (T M).

DMC Output Total Latency (T M – T D) is defined to be the difference in time from when the feed latency measurement is made and the time of the last sample in the data record. It should be obvious that the Output Total Latency is the sum of the Output Data Latency and the Output Feed Latency.

*/

update "LASTStationlatency" SET "dataLatency" = "TA" - "TD";
update "LASTStationlatency" SET "feedLatency" = "TM" - "TA";
update "LASTStationlatency" SET "totalLatency" = "TM" - "TD";


CREATE table  "LASTlatency"  AS 
SELECT DISTINCT 
	a.network || a.station || a.location || a.channel as keytext,
	a.network,
	a.station,
	a.location,
	a.channel,
	NULL::timestamp without time zone AS "TD",
	NULL::timestamp without time zone AS "TA",
	NULL::timestamp without time zone AS "TM",
	'0 sec'::INTERVAL as "dataLatency",
	'0 sec'::INTERVAL as "feedLatency",
	'0 sec'::INTERVAL as "totalLatency"
FROM "alllatency_out" a;

update "LASTlatency" SET 
	"TM" = min_tm_channel(network, station, location, channel);

update "LASTlatency" SET 
	"TD" = "a".td 
	FROM "alllatency_out" a
	WHERE
	a.network = "LASTlatency".network AND
	a.station = "LASTlatency".station AND
	a.location = "LASTlatency".location AND
	a.channel = "LASTlatency".channel AND
	a.tm= "LASTlatency"."TM";

update "LASTlatency" SET 
	"TA" = "a".ta
	FROM "alllatency_out" a
	WHERE
	a.network = "LASTlatency".network AND
	a.station = "LASTlatency".station AND
	a.location = "LASTlatency".location AND
	a.channel = "LASTlatency".channel AND
	a.tm= "LASTlatency"."TM" AND
	a.td= "LASTlatency"."TD" ;
	
	
update "LASTlatency" SET "dataLatency" = "TA" - "TD";
update "LASTlatency" SET "feedLatency" = "TM" - "TA";
update "LASTlatency" SET "totalLatency" = "TM" - "TD";

CREATE INDEX "ix_LASTStationlatency_network" ON "LASTStationlatency"("network");
CREATE INDEX "ix_LASTStationlatency_station" ON "LASTStationlatency"("station");
CREATE INDEX "ix_LASTStationlatency_TD" ON "LASTStationlatency"("TD");
CREATE INDEX "ix_LASTStationlatency_TA" ON "LASTStationlatency"("TA");
CREATE INDEX "ix_LASTStationlatency_TM" ON "LASTStationlatency"("TM");
CREATE INDEX "ix_LASTStationlatency_dl" ON "LASTStationlatency"("dataLatency");
CREATE INDEX "ix_LASTStationlatency_fl" ON "LASTStationlatency"("feedLatency");
CREATE INDEX "ix_LASTStationlatency_tl" ON "LASTStationlatency"("totalLatency");

CREATE INDEX "ix_LASTlatency_network" ON "LASTlatency"("network");
CREATE INDEX "ix_LASTlatency_station" ON "LASTlatency"("station");
CREATE INDEX "ix_LASTlatency_location" ON "LASTlatency"("location");
CREATE INDEX "ix_LASTlatency_channel" ON "LASTlatency"("channel");
CREATE INDEX "ix_LASTlatency_TD" ON "LASTlatency"("TD");
CREATE INDEX "ix_LASTlatency_TA" ON "LASTlatency"("TA");
CREATE INDEX "ix_LASTlatency_TM" ON "LASTlatency"("TM");
CREATE INDEX "ix_LASTlatency_dl" ON "LASTlatency"("dataLatency");
CREATE INDEX "ix_LASTlatency_fl" ON "LASTlatency"("feedLatency");
CREATE INDEX "ix_LASTlatency_tl" ON "LASTlatency"("totalLatency");


