#!/bin/bash
#
# Script de mise à jour d'une base budresif pour les temps de latence
# La latence considérée est la latence en entrée
# Péquegnat 2015
# Tables concernées dans budresif : voir le script SQL : init-budresifb.sql
# msi doit être accessible dans le PATH
. /home/sysop/.bash_profile
set -u
# export DESTMAILLATENCY="Catherine.Pequegnat@univ-grenoble-alpes.fr"
while [ 1 ]; do
# echo "Updating $db with last latencies..."
$HOME/scripts/feed-latency.bash | psql -c "DELETE FROM \"latency_in\"; COPY \"latency_in\" FROM STDIN USING DELIMITERS '|';"  # 1>/dev/null 2>/dev/null
psql -f $HOME/scripts/update-latency.sql  #1>/dev/null 2>/dev/null
#if [ -z $DESTMAILLATENCY ] ; then
#echo "See http://ds.iris.edu/ds/nodes/dmc/data/latency/ for Latency Measurements"
#psql -c "SELECT DISTINCT * FROM \"LASTlatency\" \
#WHERE \"totalLatency\" > CAST('119 min' AS INTERVAL) AND  \"channel\" in ('HHZ','HHN','HHE','BHZ','BHE','BHN','HNZ','HNN','HNE') \
#ORDER BY  \"network\", \"totalLatency\" DESC;" >> /tmp/resifLatency.$$
#cat /tmp/resifLatency.$$ | mail -s 'Data latency at BUD-RESIF-B' $DESTMAILLATENCY
#/bin/rm /tmp/resifLatency.$$
#fi
#sleep 120
sleep 300
done

