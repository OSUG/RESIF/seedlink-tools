--
-- "ALLlatency_out"  contient les données issues de l'analyse ponctuelle des flux sortant de rtserve.resif.fr
-- memorisation des valeurs récentes pour les temps de latences
--
\set ON_ERROR_STOP 1
--
-- backup all the latency mesures, for 2 months
-- 
INSERT INTO "ALLlatencyStore_out" (network,station,location,channel,"TD","TA","TM", "dataLatency", "feedLatency","totalLatency" )
SELECT DISTINCT 
a.network,
a.station,
a.location,
a.channel,
a."td",
a."ta",
a."tm",
a."datalatency",
a."feedlatency",
a."totallatency"
FROM "alllatency_out" a;
--
-- Delete from "alllatency_out" where tm < '2017-05-22 00:00:00';
-- 
Delete from "ALLlatencyStore_out"  WHERE current_timestamp - "TM" >= CAST ('2 month' AS INTERVAL);
--
-- Update TM, TA et TD for network|| station present in "LASTStationlatency"
--
-- update values for known streams
-- 
UPDATE "LASTStationlatency" 
SET	"TM" = "alllatency_out"."tm", 	"TD" = "alllatency_out".td , 	"TA" = "alllatency_out".ta
FROM "alllatency_out" 
WHERE 
"LASTStationlatency".keytext =  "alllatency_out"."network" || "alllatency_out"."station"
AND "alllatency_out"."tm" = min_tm_station("alllatency_out"."network", "alllatency_out"."station") ;
--
-- add values for new streams
-- 
INSERT INTO "LASTStationlatency"
SELECT DISTINCT
alllatency_out.network || alllatency_out.station,
alllatency_out.network,
alllatency_out.station,
alllatency_out."td",
alllatency_out."ta",
alllatency_out."tm",
alllatency_out."datalatency",
alllatency_out."feedlatency",
alllatency_out."totallatency"
FROM "alllatency_out" 
WHERE alllatency_out.network || alllatency_out.station NOT IN (SELECT DISTINCT "LASTStationlatency".keytext from "LASTStationlatency" )
AND alllatency_out."tm" = min_tm_station("alllatency_out"."network", "alllatency_out"."station") 
; 
--
-- update values for vanished streams (TM <-- current timestamp)
--
UPDATE "LASTStationlatency" SET 
"TM" = current_timestamp 
WHERE keytext NOT IN (SELECT DISTINCT alllatency_out.network || alllatency_out.station FROM alllatency_out);
--
-- update values for known streams
-- 
UPDATE "LASTlatency" 
SET	"TM" = "alllatency_out"."tm", "TD"="alllatency_out"."td", "TA"="alllatency_out"."ta"
FROM "alllatency_out" 
WHERE 
"LASTlatency".keytext =  "alllatency_out"."network" || "alllatency_out"."station" ||   "alllatency_out"."location" ||  "alllatency_out"."channel"
AND "alllatency_out"."tm" = min_tm_channel("alllatency_out"."network", "alllatency_out"."station", "alllatency_out"."location", "alllatency_out"."channel") ;
--
-- add values for new streams
-- 
INSERT INTO "LASTlatency"
SELECT DISTINCT
alllatency_out.network || alllatency_out.station || alllatency_out.location || alllatency_out.channel,
alllatency_out.network,
alllatency_out.station,
alllatency_out.location,
alllatency_out.channel,
alllatency_out."td",
alllatency_out."ta",
alllatency_out."tm",
alllatency_out."datalatency",
alllatency_out."feedlatency",
alllatency_out."totallatency"
FROM "alllatency_out" 
WHERE alllatency_out.network || alllatency_out.station || alllatency_out.location || alllatency_out.channel
NOT IN (SELECT DISTINCT "LASTlatency".keytext from "LASTlatency" )
AND alllatency_out."tm" = min_tm_channel("alllatency_out"."network", "alllatency_out"."station", "alllatency_out".location, "alllatency_out".channel)
; 
--
-- update values for vanished streams (TM <-- current timestamp)
--
UPDATE "LASTlatency" SET 
"TM" = current_timestamp  
WHERE keytext NOT IN (SELECT DISTINCT a.network || a.station ||  a.location || a.channel FROM alllatency_out a);

DELETE FROM "alllatency_out"; 



