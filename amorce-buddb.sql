
DELETE FROM "LASTStationlatency";
DELETE FROM "LASTlatency";


INSERT INTO  "LASTStationlatency"  SELECT DISTINCT
	a.network || a.station as keytext,
	a.network,
	a.station,
	NULL::timestamp without time zone AS "TD",
	NULL::timestamp without time zone AS "TA",
	NULL::timestamp without time zone AS "TM",
	'0 sec'::INTERVAL as "dataLatency",
	'0 sec'::INTERVAL as "feedLatency",
	'0 sec'::INTERVAL as "totalLatency"
FROM "alllatency_out" a;

update "LASTStationlatency" SET 
	"TM" = min_tm_station(network, station);

update "LASTStationlatency" SET 
	"TD" = "a".td 
	FROM "alllatency_out" a
	WHERE
	a.network = "LASTStationlatency".network AND
	a.station = "LASTStationlatency".station AND
	a.tm= "LASTStationlatency"."TM";

update "LASTStationlatency" SET 
	"TA" = "a".td 
	FROM "alllatency_out" a
	WHERE
	a.network = "LASTStationlatency".network AND
	a.station = "LASTStationlatency".station AND
	a.tm= "LASTStationlatency"."TM" AND
	a.td= "LASTStationlatency"."TD" ;


update "LASTStationlatency" SET "dataLatency" = "TA" - "TD";
update "LASTStationlatency" SET "feedLatency" = "TM" - "TA";
update "LASTStationlatency" SET "totalLatency" = "TM" - "TD";


INSERT INTO  "LASTlatency"  
SELECT DISTINCT 
	a.network || a.station || a.location || a.channel as keytext,
	a.network,
	a.station,
	a.location,
	a.channel,
	NULL::timestamp without time zone AS "TD",
	NULL::timestamp without time zone AS "TA",
	NULL::timestamp without time zone AS "TM",
	'0 sec'::INTERVAL as "dataLatency",
	'0 sec'::INTERVAL as "feedLatency",
	'0 sec'::INTERVAL as "totalLatency"
FROM "alllatency_out" a;

update "LASTlatency" SET 
	"TM" = min_tm_channel(network, station, location, channel);

update "LASTlatency" SET 
	"TD" = "a".td 
	FROM "alllatency_out" a
	WHERE
	a.network = "LASTlatency".network AND
	a.station = "LASTlatency".station AND
	a.location = "LASTlatency".location AND
	a.channel = "LASTlatency".channel AND
	a.tm= "LASTlatency"."TM";

update "LASTlatency" SET 
	"TA" = "a".ta
	FROM "alllatency_out" a
	WHERE
	a.network = "LASTlatency".network AND
	a.station = "LASTlatency".station AND
	a.location = "LASTlatency".location AND
	a.channel = "LASTlatency".channel AND
	a.tm= "LASTlatency"."TM" AND
	a.td= "LASTlatency"."TD" ;
	
	
update "LASTlatency" SET "dataLatency" = "TA" - "TD";
update "LASTlatency" SET "feedLatency" = "TM" - "TA";
update "LASTlatency" SET "totalLatency" = "TM" - "TD";


