# .bash_profile

# Get the aliases and functions
if [ -f ~/.bashrc ]; then
	. ~/.bashrc
fi

# User specific environment and startup programs

export PGHOST=postgres-geodata.ujf-grenoble.fr
export PGDATABASE=budresifb
export PGUSER="rtservice"  
export PGPASSFILE=$HOME/.pgpass

alias cp='cp -i'
alias mv='mv -i'
alias rm='rm -i'

PATH=$PATH:$HOME/.local/bin:$HOME/bin

export PATH

# add timestamps in .bash_history
export HISTTIMEFORMAT="%F %T "
