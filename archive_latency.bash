#!/bin/bash
. /home/sysop/.bash_profile
export PGDATABASE="budresifb"
export PGUSER="rtservice"
export LOGDIR=$HOME/logs/latence
mkdir -p $LOGDIR
psql -d $PGDATABASE -c "COPY \"ALLlatencyStore\" TO STDOUT USING DELIMITERS '|';" > $LOGDIR/ALLlatencyStore.`date +"%B.%Y"`
psql -d $PGDATABASE -c "DELETE FROM \"ALLlatencyStore\" WHERE current_timestamp - \"TD\" >= '6 days'::INTERVAL;"
gzip $LOGDIR/ALLlatencyStore.`date +"%B.%Y"`
