#!/bin/bash
# [22 aout 2012, Pequegnat] : creation du script
# 
# Identifie la présence d'overlpa de durée > 1h en sortie du système temps reel du noeud B (rtserve.resif.fr)
#
# Exécuté par cron sur la machine de monitoring applicatif
# 
# Les données diffusées par le noeud B sont collectées par  slinktool -A ~sysop/work/monitoring/seedlink/overlap/%n.%s.%Y.%j rtserve.resif.fr
# 
# Les données 
#
#		et déposées sous $HOME/work/datacheck/loop
# 
# Le script qui suit contrôle qu'il n'y a pas trop d'overlaps de données, et que si il y en a, leur taille reste stable
# En cas d'overlaps trop important, le tampon circulaire à partir duquel les données sont diffusées, et l'index du scan,
# peuvent être corrompus. Les interventions sont alors manuelles FIXME Catherine : mettre le lien
# Les overlaps sont repérés par une analyse de l'output de la commande msi
# L'output de msi est passé dans la base de données du temps réel : budresifb, et le traitement est fait en base
#
# [21 février 2018, CP] reprise du script pour mise en place sur resif8
set -u 
#-------------------------------------------------------------------------------------------------------------------------------
# DEBUT Note à destination de Gregory pour commencer les modifications du script en vue d'une remontée des problèmes dans zabbix
# ------------------------------------------------------
# Voir également https://wiki-geodata.obs.ujf-grenoble.fr/doku.php?id=geodata:systemes_et_reseaux:monitoring_seedlink
#
# TODO : (1) consolider le script  qui remplace les lignes non commentées
#			(2) l'alerte envoyée par mail à DESTMAIL est à remplacer par une ou des notifications à Zabbix
#
#-------------------------------------------------------------------------------------------------------------------------------
#  FIN Note à destination de Gregory pour commencer les modifications de scripts en vue d'une remontée des problèmes dans zabbix
# ------------------------------------------------------
# 
# set -x
STARTDATE=$(date +%s)
export PGHOST=postgres-geodata.ujf-grenoble.fr
export PGDATABASE=budresifa
export PGUSER=budresifa
export PATH=/usr/local/bin:/bin:/usr/bin:/usr/local/sbin:/usr/sbin:/usr/pgsql-9.6/bin/:/home/sysop/.local/bin:/home/sysop/bin
HOST=$(hostname --fqdn)
INCOMINGDIR="$HOME/work/monitoring/seedlink/overlap/resif-vm42"; mkdir -p $INCOMINGDIR
LOGDIR=/home/sysop/logs; mkdir -p $LOGDIR
OUTPUTFILE=$LOGDIR/resif-vm42.out
DESTMAIL="Catherine.Pequegnat@univ-grenoble-alpes.fr,Jonathan.Schaeffer@univ-grenoble-alpes.fr"
export MY_HOME="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
#
[[ -d $INCOMINGDIR ]] && cd $INCOMINGDIR
psql  -c "DELETE FROM go_in;" ; 
[[ $? != 0 ]] && exit 2
msi -tg * | sed -e '/Source/d' | sed -e '/Total/d' | \
awk '{printf("%s\t%s\t%s\t%s\t%s\t%s\n",$1,$2,$3,$4,$5,$6)}' | \
psql -c "COPY go_in FROM stdin;"
[[ $? != 0 ]] && exit 2

psql -f ${MY_HOME}/check-overlap.sql
[[ $? != 0 ]] && exit 2
psql -c "COPY go_out TO STDOUT;" > $OUTPUTFILE
[[ $? != 0 ]] && exit 2

#-------------------------------------------------------------------------------------------------------------------------------
# DEBUT Note à destination de Gregory pour commencer les modifications du script en vue d'une remontée des problèmes dans zabbix
# Pour le moment, l'analyse des problèmes signalés dans le mail est faite 'à l'oeil'
# On peut essayer d'automatiser un peu, en travallant sur le :
# 	le nombre de réseau concernés
#	le nom de station concernées
#  la durée des overlaps (plus compliqué)
#  je suggère que pour le moment, on en reste à quelque chose de très simple :
#        			SI $LOGDIR contient plus de 10 lignes, il faut remonter une alerte sévère
#						SI $LOGDIR contient moins de 10 lignes, il faut remonter un warning
# ... et on affinera ensuite

# ------------------------------------------------------
OVERLAPPED_CHANNELS=0
if [[ -s $OUTPUTFILE ]] ; then
	OVERLAPPED_CHANNELS=$(wc -l $OUTPUTFILE|awk '{print $1}')
	echo >> $OUTPUTFILE
	echo "Voir https://wiki-new.osug.fr/!isterre-geodata/resif/systemes/monitoring/flux_temps_reel#presence_d_overlaps_sur_resif-vm42_rapsismob" >> $OUTPUTFILE
	cat $OUTPUTFILE | mail -s "Attention : $OVERLAPPED_CHANNELS canaux en overlap sur le nœud A" $DESTMAIL
fi

echo "Envoi de données à zabbix : "
echo "zabbix_sender -z osug-monitoring.ujf-grenoble.fr -s resif8 -k overlap-resif-b -o $OVERLAPPED_CHANNELS"
zabbix_sender -z osug-monitoring.ujf-grenoble.fr -s $HOST -k overlap-resif-a -o $OVERLAPPED_CHANNELS

ENDDATE=$(date +%s)
ELAPSED=$(expr $ENDDATE - $STARTDATE)
zabbix_sender -z osug-monitoring.ujf-grenoble.fr -s $HOST -k overlap-elapsed-resif-a -o $ELAPSED
