-- check-output-tqr.sql
--
DELETE FROM "go_out";
DELETE FROM "go_in" WHERE "gapoverlap" = '==';
DELETE FROM "go_in" WHERE "gapoverlap" NOT LIKE '-%';  -- on ne s'interesse qu'aux overlaps, signalés par le signe '-'
INSERT INTO "go_out" SELECT DISTINCT *  FROM "go_in" WHERE "gapoverlap" LIKE '%h';
INSERT INTO "go_out" SELECT DISTINCT *  FROM "go_in" WHERE "gapoverlap" LIKE '%d';


