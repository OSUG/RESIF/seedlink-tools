#!/bin/env python
# -*- coding: utf-8 -*-
# Test des overlaps du temps réel à partir d'un fichier de configuration config.yml

import logging
import logging.config
import logging.handlers
import os
import time
import re
import socket
import yaml
import argparse
from subprocess import Popen, PIPE
from pyzabbix import ZabbixMetric, ZabbixSender

script_path = os.path.dirname(os.path.abspath(__file__)) + "/"
logger = logging.getLogger("check-overlap")
parser = argparse.ArgumentParser(description="Scan seedlinks for overlaps.")
parser.add_argument(
    "--seedlink", type=str, help="a seedlink name as defined in the configuration file."
)
args = parser.parse_args()


def main():
    with open(script_path + "config.yml", "r") as ymlfile:
        cfg = yaml.load(ymlfile)
    logger.debug(cfg)

    msi_path = cfg["msi_path"]
    logger.debug("Chemin MSI : %s" % msi_path)
    zabbix_packet = []
    re_junk = re.compile(r"(Source)|(==)|(Total)")
    re_big_overlap = re.compile(r"^-.*[dh]$")

    for node_config in cfg["seedlinks"]:
        # Check if we have to do this seedlink :
        if args.seedlink != None and args.seedlink != node_config["name"]:
            continue
        logger.debug(args.seedlink)
        overlaps = {}
        # Let's work on each section as a node
        # Do we have data from the node ?
        # Test if directory exists
        incomingdir = node_config["incomingdir"]
        if os.path.isdir(incomingdir) != True:
            logger.error("incomingdir %s does not exist." % incomingdir)
            try:
                os.makedirs(incomingdir)
            except Exception as e:
                logger.error(e)
                continue
        filelist = os.listdir(incomingdir)
        if not filelist:
            logger.warning("incomingdir %s is empty" % incomingdir)
            continue
        # Run MSI on all data with -tg option
        logger.info("Run MSI : %s" % msi_path)
        # List files in incomingdir
        msicommand = [
            incomingdir + "/" + f
            for f in os.listdir(incomingdir)
            if os.path.isfile(os.path.join(incomingdir, f))
        ]
        msicommand.insert(0, "-tg")
        msicommand.insert(0, msi_path)
        starttime = time.time()
        logger.debug(" ".join(msicommand))
        msirun = Popen(msicommand, stdout=PIPE)
        (output, err) = msirun.communicate()
        if err != None:
            logger.error("msi error %s" % err)
            exit(2)
        # Analyse output
        #    Source                Start sample             End sample        Gap  Hz  Samples
        # FR_OGMO_00_HNN    2018,076,00:00:01.983300 2018,078,09:18:53.875000  ==  120 24759828
        # Total: 9 trace(s) with 9 segment(s)
        logger.info("[%s] Analysing overlaps" % node_config["name"])
        for line in output.decode("utf-8").splitlines():
            if re_junk.search(line):
                continue
            (source, start, end, gap, hz, samples) = line.split()
            logger.debug("Source  : %s" % source)
            logger.debug("Start   : %s" % start)
            logger.debug("End     : %s" % end)
            logger.debug("Gap     : %s" % gap)
            logger.debug("Hz      : %s" % hz)
            logger.debug("Samples : %s" % samples)

            if re_big_overlap.search(gap):
                logger.info("OVERLAP %s on %s" % (gap, source))
                source = "_".join(source.split("_")[:2])  # => FR_OGMO
                overlaps[source] = {
                    "start": start,
                    "end": end,
                    "gap": gap,
                    "hz": hz,
                    "samples": samples,
                }
        # We collected the overlapped channels
        # Now sum up all overlaps to get an idea of the situation
        total_overlap = 0.0  # 0 hours
        for k, o in overlaps.items():
            gap = 0.0
            try:
                gap = float(o["gap"][1:-1])
            except ValueError:
                logger.warning("Oops!  %s was no valid number. Skipping." % o["gap"])
            if o["gap"].endswith("d"):
                total_overlap = total_overlap + gap * 24
            else:
                total_overlap = total_overlap + gap
        elapsed_time = int(time.time() - starttime)
        logger.info(
            "[%s] %d channels overlapping." % (node_config["name"], len(overlaps))
        )
        logger.info("[%s] %f hours overlapping." % (node_config["name"], total_overlap))
        logger.debug("Temps d'exécution : %d" % elapsed_time)
        zabbix_packet.append(
            ZabbixMetric(
                cfg["zabbix_agent"], node_config["zabbix_overlap_key"], len(overlaps)
            )
        )
        zabbix_packet.append(
            ZabbixMetric(
                cfg["zabbix_agent"], node_config["zabbix_time_key"], elapsed_time
            )
        )
        zabbix_packet.append(
            ZabbixMetric(
                cfg["zabbix_agent"],
                node_config["zabbix_totaloverlap_key"],
                int(total_overlap),
            )
        )

    # Send data to zabbixtrapper
    for z in zabbix_packet:
        logger.debug(z)
    result = ZabbixSender(zabbix_server=cfg["zabbix_server"]).send(zabbix_packet)
    logger.info(result)


if __name__ == "__main__":
    main()

# vim: expandtab shiftwidth=4 softtabstop=4
