#!/bin/bash
#  
# HISTORIQUE DU FICHIER -----------------------------------
# Vérification de l'insertion en base resifInv-Prod des fichiers de données du jour, dans la table rbud
# [Pequegnat, création du fichier]
#
# set -x
#-------------------------------------------------------------------------------------------------------------------------------
# DEBUT Note à destination de Gregory pour commencer les modifications de scripts en vue d'une remontée des problèmes dans zabbix
# ------------------------------------------------------
# Voir également https://wiki-geodata.obs.ujf-grenoble.fr/doku.php?id=geodata:systemes_et_reseaux:monitoring_seedlink
#
# TODO : écrire un script (bash, python..) qui remplace les lignes non commentées
#
# Suppression des accès directs à la base de données resifInv-Prod. 
# Les vérifications de l'enregistrement des données temps réel récentes dans le noeud B se font en exécutant une requête au webservice dataselect
# avec les paramètres : network= et start=
# Remarque : cette requête prend du temps
# Les valeurs des paramètres sont :
## network="FR,RA,RD,ND,MT,G,GL,WI,PF,MQ,CL"
## start= ... date UTC au format "YYYY-­MM-DDTHH:MM:SS" qui a pour valeur : NOW - 600 secondes .
# Attention, trouver le moyen le plus portable de calculer cette date, python, bash (date and co), ... 
#
# exécuter la requête dataselect de  données, ecrire les données pour analyse, récupérer le code retour
#     attention au proxy
## wget --no-proxy "http://ws.resif.fr/fdsnws/dataselect/1/query?network=$network&start=$start&channel=??Z&nodata=404" -O ~/work/onehour.mseed
#
# Tester la réponse  de la requête. Si  404 : alerte
# NB : voir si test du code retour de wget, et ce qu'on en fait
# 
# analyser très sommairement le fichier miniseed ~/work/onehour.mseed récupéré en utilisant l'outil 'msi'
# 
# msi -h pour le help de la commande
# 
## msi ~/work/onehour.mseed ~/work/onehour.mseed
#
## Si la requête renvoie moins de ligne qu'il n'y a de code réseau, émettre un warning
#-------------------------------------------------------------------------------------------------------------------------------
#  FIN Note à destination de Gregory pour commencer les modifications de scripts en vue d'une remontée des problèmes dans zabbix
# ------------------------------------------------------


export PGHOST="postgres-geodata.ujf-grenoble.fr"
export PGUSER="resifinvprod"
export PGPASSFILE="$HOME/.pgpass"
export PGDATABASEprod="resifInv-Prod"
export DATESTRING="%`date +%Y.%j`" 

date "+%Y-%m-%dT%H:%M:%S"
2018-02-21T16:42:27

#
# Les fichiers du jours rentrent-il dans 'RBUD'
echo --------------------------------------
echo "Date : `date`, `date +%Y.%j`"
echo --------------------------------------
echo "Data incomping for day : `date +%Y.%j`"
psql -U $PGUSER -d $PGDATABASEprod -t -c "SELECT DISTINCT  \"network\" FROM  \"RBud\" WHERE \"fileName\" LIKE '$DATESTRING' ORDER BY \"network\";"
echo --------------------------------------
echo "Filenames for day : `date +%Y.%j`"
psql -U $PGUSER -d $PGDATABASEprod -t -c "SELECT DISTINCT  \"network\", \"fileName\", \"starttime\" FROM  \"RBud\" WHERE \"fileName\" LIKE '$DATESTRING' ORDER BY \"network\";"
	
