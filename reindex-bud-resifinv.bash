#!/bin/bash
# 
# Reindex la table "RBud dans les bases resifInv
#
# CE FICHIER CONTIENT UN MOT DE PASSE : chmod 700 recommandé.
#
# 
# [Pequegnat, création du fichier]
#
#
# LIMITATIONS ---------------------------------------------
#
#
# 
# set -x
. ~/.bash_profile
export PGHOST="postgres-geodata.ujf-grenoble.fr"
export PGUSER="bdsis"
export PGPASSFILE="$HOME/.pgpass"
export PGDATABASEprod="resifInv-Prod"
psql -d $PGDATABASEprod -c "REINDEX TABLE \"RBud\";"

