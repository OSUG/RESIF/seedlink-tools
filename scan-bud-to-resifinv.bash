#!/bin/bash
# set -x
# $HOME/bin/scan-bud-to-resifinv.bash
## [Dec 7, 2016 C. Péquegnat ] : le script est installé sur resif-vm31, et légèrement modifié pour remplacer momentanément, en cron
##		la notification de la présence d'un fichier de données à l'inventaire des données sur le noeud B
##
# Mise à jour de resifInv par journée.
. $HOME/.bash_profile
set -u
export "LOGFILE"="$HOME/logs/resif-repair-tr.log"; 

function echolog() { echo "$@" 1>>$LOGFILE; }

# check RUNMODE exists
trap "echo RUNMODE environnement variable must be set to 'test' or 'production'" EXIT
echo $RUNMODE > /dev/null
trap - EXIT
#
export YEAR=`date +%Y`; export DAY=`date +%j`; 
#
case $RUNMODE in
	"production") 
	export PGDATABASE="resifInv-Prod"
	export PGUSER="bdsis"
   export PSQL="psql"
	export ROOT="/mnt/auto/archive/rawdata/bud"
	;;
	"test")
	export PGDATABASE="resifInv-Dev"
	export PGUSER="bdsis"
	export PSQL="psql" 
	export ROOT="/mnt/auto/archive/rawdata/bud"
	;;
esac	
#
[[ ! -d "${ROOT}/${YEAR}" ]] && echolog "`date +%FT%T` [$$] ERROR : ${ROOT}/${YEAR} not found" && exit 128
#
psql -d $PGDATABASE -c "CREATE TABLE IF NOT EXISTS \"temp_scan_bud\" (filename TEXT);  DELETE FROM \"temp_scan_bud\";" 1>>$LOGFILE 2>>$LOGFILE
CR=$?
[[ "$CR" -ne "0" ]] && echolog "`date +%FT%T` [$$] ERROR : CREATE TABLE IF NOT EXISTS \"temp_scan_bud\" (filename TEXT) return $CR" && exit 128
cd "${ROOT}/${YEAR}"
#
psql -d $PGDATABASE -c "CREATE TABLE IF NOT EXISTS \"temp_scan_bud\" (filename TEXT);  DELETE FROM \"temp_scan_bud\";" 1>>$LOGFILE 2>>$LOGFILE
find . -type f -name "*${YEAR}.${DAY}" -print | \
	psql -d $PGDATABASE -c "COPY \"temp_scan_bud\" FROM STDIN; \
	UPDATE \"temp_scan_bud\" SET filename = basename (filename,'/'); \
	DELETE FROM \"temp_scan_bud\" USING \"RBud\" WHERE \"RBud\".\"fileName\" = filename;
	COPY \"temp_scan_bud\" TO STDOUT;" | while read line; do
	export "filename"=`ls */*/*/${line}`
	export "name"=`basename $filename`
	export "net"=`echo $name | cut -d'.' -f 1`
	export "station"=`echo $name | cut -d'.' -f 2`
	export "location"=`echo $name | cut -d'.' -f 3`
	export "channel"=`echo $name | cut -d'.' -f 4`
	export "type"=`echo $name | cut -d'.' -f 5`
	export "year"=`echo $name | cut -d'.' -f 6`
	export "day"=`echo $name | cut -d'.' -f 7`
	export "quality"=`msi -p $filename | grep $net | cut -d',' -f3 | sort -u | sed "s/ //g"`
	export starttime=`msi -tg $filename | grep $net | sed "/Source/d" | head -n 1 | awk '{print $2}' | sed "s/ //g"`
	echolog "`date +%FT%TZ` [$$] NOTICE  : $filename, $name, $net, $quality, $starttime, $day "
	echo "`date +%FT%TZ` [$$] NOTICE  : $filename, $name, $net, $quality, $starttime, $day "
# 
#
###
# 
# En fait il n'est pas forcément intelligent de stocker dans RBud la valeur starttime au premier accès, car cette dernière peut évoluer "dans le passé" :
# seedlink transmet toujours d'abord les données les plus récentes (orienté "alert"). Donc il vaut mieux, même si c'est un peu faux,
# attacher la date théorique de début de jour au fichier, afin de ne pas rater la diffusion des segments arrivés après coup 
# 
#if [ "$type" = "D" ] 
# seuls les flux de DATA (type = 'D') sont répertoriés dans la base de données; les logs, les opaque data ne sont pas prise en compte

#	then
#	echolog "`date +%FT%TZ` [$$] NOTICE  : integratig $filename "
#	psql -d $PGDATABASE -c " \
#	INSERT INTO  \"RBud\" (\"fileName\", \"network\", \"station\", \"location\" ,\"channel\",  \"quality\", \"sizeBlock\", \"year\", \"starttime\", \"endtime\" ) \
#	SELECT '$name', '$net', '$station', '$location', '$channel', '$quality', '512',  '$year', \
#	TO_TIMESTAMP('$starttime','YYYY,DDD,HH24:MN:SS.US'), \
#   TO_TIMESTAMP(SPLIT_PART('$name','.', 6) || '-' ||SPLIT_PART('$name','.', 7), 'YYYY-DDD'),  \
#	TO_TIMESTAMP(SPLIT_PART('$name','.', 6) || '-' ||SPLIT_PART('$name','.', 7), 'YYYY-DDD') + CAST ('24h' AS INTERVAL) \
#	WHERE \
#	NOT EXISTS (SELECT  \"b\".\"fileName\" FROM  \"RBud\" \"b\" WHERE \"fileName\" = '$name') ;" 1>>$LOGFILE 2>>$LOGFILE
# 
if [ "$type" = "D" ] 
# seuls les flux de DATA (type = 'D') sont répertoriés dans la base de données; les logs, les opaque data ne sont pas prise en compte
	then
	echolog "`date +%FT%TZ` [$$] NOTICE  : integratig $filename "
	psql -d $PGDATABASE -c " \
	INSERT INTO  \"RBud\" (\"fileName\", \"network\", \"station\", \"location\" ,\"channel\",  \"quality\", \"sizeBlock\", \"year\", \"starttime\", \"endtime\" ) \
	SELECT '$name', '$net', '$station', '$location', '$channel', '$quality', '512',  '$year', \
   TO_TIMESTAMP(SPLIT_PART('$name','.', 6) || '-' ||SPLIT_PART('$name','.', 7), 'YYYY-DDD'),  \
	TO_TIMESTAMP(SPLIT_PART('$name','.', 6) || '-' ||SPLIT_PART('$name','.', 7), 'YYYY-DDD') + CAST ('24h' AS INTERVAL) \
	WHERE \
	NOT EXISTS (SELECT  \"b\".\"fileName\" FROM  \"RBud\" \"b\" WHERE \"fileName\" = '$name') ;" 1>>$LOGFILE 2>>$LOGFILE
#
	CR=$?
	[[ "$CR" -ne "0" ]] && echolog "`date +%FT%TZ` [$$] ERROR : INSERT INTO  \"RBud\" (\"fileName\" ... WHERE NOT EXISTS ... return $CR" 
#
#  L'information qui suit dans le fichier de log est importante car elle permet de reconstituer éventuellement une liste de comande d'insertion
#
	echolog "`date +%FT%TZ` [$$] NOTICE  :  psql -d $PGDATABASE -c \"INSERT INTO  \"RBud\" (\"fileName\", \"network\", \"station\", \"location\" ,\"channel\",  \"quality\", \"sizeBlock\", \"year\", \"starttime\", \"endtime\" ) \
	SELECT '$name', '$net', '$station', '$location', '$channel', '$quality', '512',  '$year', \
	TO_TIMESTAMP('$starttime','YYYY,DDD,HH24:MN:SS.US'), \
	TO_TIMESTAMP(SPLIT_PART('$name','.', 6) || '-' ||SPLIT_PART('$name','.', 7), 'YYYY-DDD') + CAST ('24h' AS INTERVAL) \
	WHERE \
	NOT EXISTS (SELECT  \"b\".\"fileName\" FROM  \"RBud\" \"b\" WHERE \"fileName\" = '$name') ;\" "
#	
# Check if exists
#
	# echo "psql -d $PGDATABASE -t -c \"SELECT DISTINCT * FROM \"RBud\" WHERE \"fileName\" = '$name';"
	#echolog "`date +%FT%TZ` [$$] NOTICE  : psql -d $PGDATABASE -t -c \"SELECT DISTINCT * FROM \"RBud\" WHERE \"fileName\" = '$name';"
	#psql -d $PGDATABASE -t -c "SELECT DISTINCT * FROM \"RBud\" WHERE \"fileName\" = '$name';"
	#CR=$?
	#[[ "$CR" -ne "0" ]] && echolog "`date +%FT%TZ` [$$] ERROR : psql SELECT $name return $CR" && exit 128
#
	else
echolog "`date +%FT%TZ` [$$] NOTICE : $name not integrated"
fi
done

